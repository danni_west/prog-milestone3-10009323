﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10009323
{
    public class Customer
    {
        public static string Name;
        public static string Number;

        public Customer(string _name, string _number)
        {
            Name = _name;
            Number = _number;
        }
    }
    public class program
    {

        public static void Main(string[] args)
        {

            Console.WriteLine("Welcome to The Pizza Shop.");
            Console.WriteLine();

            Console.WriteLine("Please enter a name that will be attached to your order..");
            var a = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Please enter your phone number..");
            var b = Console.ReadLine();
            Console.WriteLine();

            Customer one = new Customer($"{a}", $"{b}");

            Console.WriteLine($"Hello {Customer.Name}");

            Console.WriteLine("Press <ENTER> to begin ordering..");
            Console.ReadLine();
            menu();

        }

        //***************************
        //*							*
        //* 	MAIN MENU     		*
        //*							*
        //***************************


        public static void menu()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("**************************************************");
            Console.WriteLine("                    MAIN MENU                   ");
            Console.WriteLine("                                                ");
            Console.WriteLine("    To order a pizza, please press \"p\".       ");
            Console.WriteLine("                                               ");
            Console.WriteLine("    To order a drink, please press \"d\".       ");
            Console.WriteLine("                                               ");
            Console.WriteLine("    To complete your order, please press \"c\". ");
            Console.WriteLine("                                               ");
            Console.WriteLine("**************************************************");

            var userinput = Console.ReadLine();

            if (userinput == "p")
            {
                Console.Clear();
                pizza();
            }

            else if (userinput == "d")
            {
                Console.Clear();
                drink();
            }

            else if (userinput == "c")
            {
                Console.Clear();
                complete();
            }

            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that's not one of the options.");
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to return to the main menu..");
                Console.ReadLine();
                menu();
            }
        }

        //***************************
        //*							*
        //* 	PIZZA MENU  		*
        //*							*
        //***************************


        public class Pizzamenu
        {
            public string Type;
            public string Size;
            public int Price;

            public static string TypeM = "Meatlovers";
            public static string TypeC = "Cheese";
            public static string TypeH = "Hawaiian";
            public static string TypeV = "Vegetarian";

            public static string small = "Small";
            public static string medium = "Medium";
            public static string large = "Large";

            public static int Small = 5;
            public static int Medium = 7;
            public static int Large = 9;

            public static List<Tuple<String, String, int>> Pizzapizza = new List<Tuple<String, String, int>>();

            public Pizzamenu(string _type, string _size, int _price)
            {
                Type = _type;
                Size = _size;
                Price = _price;
            }
        }

        public static void pizza()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("*****************************");
            Console.WriteLine("*        PIZZA MENU         *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*   Please select a pizza   *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*      1. Meatlovers        *");
            Console.WriteLine("*      2. Cheese            *");
            Console.WriteLine("*      3. Hawaiian          *");
            Console.WriteLine("*      4. Vegetarian        *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*****************************");

            var pizzaselection = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(pizzaselection, out a);

            if (value == true)
            {
                int p = int.Parse(pizzaselection);

                /// Meatlovers Pizza ///

                if (p == 1)
                {
                    Console.Clear();
                    Console.WriteLine("You have selected a meatlovers pizza.");
                    Console.WriteLine();
                    Console.WriteLine("Please press..");
                    Console.WriteLine();
                    Console.WriteLine("\"s\" for small ($5).");
                    Console.WriteLine("\"m\" for medium ($7).");
                    Console.WriteLine("\"l\" for large ($9).");
                    var sizeselection = Console.ReadLine();

                    if (sizeselection == "s")
                    {
                        Console.Clear();
                        Console.WriteLine("A small meatlovers pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeM, Pizzamenu.small, Pizzamenu.Small));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();

                        menu();
                    }

                    else if (sizeselection == "m")
                    {
                        Console.Clear();
                        Console.WriteLine("A medium meatlovers pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeM, Pizzamenu.medium, Pizzamenu.Medium));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();

                        menu();
                    }

                    else if (sizeselection == "l")
                    {
                        Console.Clear();
                        Console.WriteLine("A large meatlovers pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeM, Pizzamenu.large, Pizzamenu.Large));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();

                        menu();
                    }

                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Sorry, that's not one of the options.");
                        Console.WriteLine();
                        Console.WriteLine("Press <ENTER> to return to the pizza menu..");
                        Console.ReadLine();

                        pizza();
                    }
                }

                /// Cheese Pizza ///

                else if (p == 2)
                {
                    Console.Clear();
                    Console.WriteLine("You have selected a cheese pizza.");
                    Console.WriteLine();
                    Console.WriteLine("Please press..");
                    Console.WriteLine();
                    Console.WriteLine("\"s\" for small ($5).");
                    Console.WriteLine("\"m\" for medium ($7).");
                    Console.WriteLine("\"l\" for large ($9).");
                    var sizeselection = Console.ReadLine();

                    if (sizeselection == "s")
                    {
                        Console.Clear();
                        Console.WriteLine("A small cheese pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeC, Pizzamenu.small, Pizzamenu.Small));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else if (sizeselection == "m")
                    {
                        Console.Clear();
                        Console.WriteLine("A medium cheese pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeC, Pizzamenu.medium, Pizzamenu.Medium));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else if (sizeselection == "l")
                    {
                        Console.Clear();
                        Console.WriteLine("A large cheese pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeC, Pizzamenu.large, Pizzamenu.Large));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Sorry, that's not one of the options.");
                        Console.WriteLine();
                        Console.WriteLine("Press <ENTER> to return to the pizza menu..");
                        Console.ReadLine();
                        pizza();
                    }
                }

                /// Hawaiian Pizza ///

                else if (p == 3)
                {
                    Console.Clear();
                    Console.WriteLine("You have selected a hawaiian pizza.");
                    Console.WriteLine();
                    Console.WriteLine("Please press..");
                    Console.WriteLine();
                    Console.WriteLine("\"s\" for small ($5).");
                    Console.WriteLine("\"m\" for medium ($7).");
                    Console.WriteLine("\"l\" for large ($9).");
                    var sizeselection = Console.ReadLine();

                    if (sizeselection == "s")
                    {
                        Console.Clear();
                        Console.WriteLine("A small hawaiian pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeH, Pizzamenu.small, Pizzamenu.Small));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else if (sizeselection == "m")
                    {
                        Console.Clear();
                        Console.WriteLine("A medium hawaiian pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeH, Pizzamenu.medium, Pizzamenu.Medium));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else if (sizeselection == "l")
                    {
                        Console.Clear();
                        Console.WriteLine("A large hawaiian pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeH, Pizzamenu.large, Pizzamenu.Large));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Sorry, that's not one of the options.");
                        Console.WriteLine();
                        Console.WriteLine("Press <ENTER> to return to the pizza menu..");
                        Console.ReadLine();
                        pizza();
                    }
                }

                /// Vegetarian Pizza ///

                else if (p == 4)
                {
                    Console.Clear();
                    Console.WriteLine("You have selected a vegetarian pizza.");
                    Console.WriteLine();
                    Console.WriteLine("Please press..");
                    Console.WriteLine();
                    Console.WriteLine("\"s\" for small ($5).");
                    Console.WriteLine("\"m\" for medium ($7).");
                    Console.WriteLine("\"l\" for large ($9).");
                    var sizeselection = Console.ReadLine();

                    if (sizeselection == "s")
                    {
                        Console.Clear();
                        Console.WriteLine("A small vegetarian pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeV, Pizzamenu.small, Pizzamenu.Small));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else if (sizeselection == "m")
                    {
                        Console.Clear();
                        Console.WriteLine("A medium vegetarian pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeV, Pizzamenu.medium, Pizzamenu.Medium));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else if (sizeselection == "l")
                    {
                        Console.Clear();
                        Console.WriteLine("A large vegetarian pizza has been added to your order.");
                        Console.WriteLine();
                        Pizzamenu.Pizzapizza.Add(Tuple.Create(Pizzamenu.TypeM, Pizzamenu.large, Pizzamenu.Large));
                        Console.WriteLine("Press <ENTER> to continue ordering..");
                        Console.ReadLine();
                        menu();
                    }

                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Sorry, that's not one of the options.");
                        Console.WriteLine();
                        Console.WriteLine("Press <ENTER> to return to the pizza menu..");
                        Console.ReadLine();
                        pizza();
                    }
                }

                else
                {
                    Console.Clear();
                    Console.WriteLine("Sorry, that's not one of the options.");
                    Console.WriteLine();
                    Console.WriteLine("Press <ENTER> to return to the pizza menu..");
                    Console.ReadLine();
                    pizza();
                }
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that's not one of the options.");
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to return to the pizza menu..");
                Console.ReadLine();
                pizza();
            }
        }

        //***************************
        //*							*
        //* 	DRINKS MENU  		*
        //*							*
        //***************************

        public class Drinksmenu
        {
            public string Type;
            public int Price;

            public static string TypeW = "Water";
            public static string TypeJ = "Fruit Juice";
            public static string TypeCo = "Cola";
            public static string TypeS = "Sprite";

            public static int watfj = 2;
            public static int fizzy = 3;

            public static List<Tuple<String, int>> Liquid = new List<Tuple<String, int>>();

            public Drinksmenu(string _type, int _price)
            {
                Type = _type;
                Price = _price;
            }
        }

        public static void drink()
        {
            List<Drinksmenu> list = new List<Drinksmenu>();

            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("*****************************");
            Console.WriteLine("*        DRINKS MENU        *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*   Please select a drink   *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*     1. Water - $2         *");
            Console.WriteLine("*     2. Fruit Juice - $2   *");
            Console.WriteLine("*     3. Cola - $3          *");
            Console.WriteLine("*     4. Sprite - $3        *");
            Console.WriteLine("*                           *");
            Console.WriteLine("*****************************");

            var drinkselection = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(drinkselection, out a);

            if (value == true)
            {
                int d = int.Parse(drinkselection);

                if (d == 1)
                {
                    Console.Clear();
                    Console.WriteLine("Water ($2), has been added to your order.");
                    Console.WriteLine();
                    Drinksmenu.Liquid.Add(Tuple.Create(Drinksmenu.TypeW, Drinksmenu.watfj));
                    Console.WriteLine("Press <ENTER> to continue ordering..");
                    Console.ReadLine();
                    menu();
                }

                else if (d == 2)
                {
                    Console.Clear();
                    Console.WriteLine("Fruit juice ($2), has been added to your order.");
                    Console.WriteLine();
                    Drinksmenu.Liquid.Add(Tuple.Create(Drinksmenu.TypeJ, Drinksmenu.watfj));
                    Console.WriteLine("Press <ENTER> to continue ordering..");
                    Console.ReadLine();
                    menu();
                }

                else if (d == 3)
                {
                    Console.Clear();
                    Console.WriteLine("Cola ($3), has been added to your order.");
                    Console.WriteLine();
                    Drinksmenu.Liquid.Add(Tuple.Create(Drinksmenu.TypeCo, Drinksmenu.fizzy));
                    Console.WriteLine("Press <ENTER> to continue ordering..");
                    Console.ReadLine();
                    menu();
                }

                else if (d == 4)
                {
                    Console.Clear();
                    Console.WriteLine("Sprite ($3), has been added to your order.");
                    Console.WriteLine();
                    Drinksmenu.Liquid.Add(Tuple.Create(Drinksmenu.TypeS, Drinksmenu.fizzy));
                    Console.WriteLine("Press <ENTER> to continue ordering..");
                    Console.ReadLine();
                    menu();
                }

                else
                {
                    Console.Clear();
                    Console.WriteLine("Sorry, that's not one of the options.");
                    Console.WriteLine();
                    Console.WriteLine("Press <ENTER> to return to the drinks menu..");
                    Console.ReadLine();
                    drink();
                }
            }

            else
            {
                Console.Clear();
                Console.WriteLine("Sorry, that's not one of the options.");
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to return to the drinks menu..");
                Console.ReadLine();
                drink();
            }
        }

        //***************************
        //*							*
        //* COMPLETION OF ORDER		*
        //*							*
        //***************************

        public static void complete()

        {
            Console.WriteLine("Your order details are listed below..");
            Console.WriteLine();
            Console.WriteLine($"Order name: {Customer.Name}. Phone number: {Customer.Number}.");
            Console.WriteLine();

            foreach(var x in Pizzamenu.Pizzapizza)
            {
                Console.WriteLine($"{x.Item1}, {x.Item2}, ${x.Item3}");
            }

            Console.WriteLine();

            foreach (var x in Drinksmenu.Liquid)
            {
                Console.WriteLine($"{x.Item1}, ${x.Item2}");
            }

            Console.WriteLine();
            Console.WriteLine("Please press \"y\" to confirm this order or \"n\" to return to the menu and add more items.");
            var confirmation = Console.ReadLine();

            if (confirmation == "y")
            {
                payment();
            }

            else if (confirmation == "n")
            {
                menu();
            }

            else
            {
                Console.WriteLine("Sorry that's not one of the options, please try again");
                complete();
            }
        }


        //***************************
        //*							*
        //* 	PAYMENT OF ORDER	*
        //*							*
        //***************************

        public static void payment()


        {
            Console.Clear();
            Console.WriteLine("Thank you for ordering from The Pizza Shop.");
            Console.WriteLine();
            Console.WriteLine("Your order details are listed below..");
            Console.WriteLine();

            Console.WriteLine($"Order name: {Customer.Name}. Phone number: {Customer.Number}.");
            Console.WriteLine();

            int pizzatotal = 0;
            int drinktotal = 0;

            foreach (var x in Pizzamenu.Pizzapizza)
            {
                Console.WriteLine($"{x.Item1}, {x.Item2}, ${x.Item3}");
                pizzatotal += x.Item3;
            }

            Console.WriteLine();

            foreach (var x in Drinksmenu.Liquid)
            {
                Console.WriteLine($"{x.Item1}, ${x.Item2}");
                drinktotal += x.Item2;
            }

            var total = pizzatotal + drinktotal;

            Console.WriteLine();
            Console.WriteLine($"The total for your order is ${total}");
            Console.WriteLine();
            Console.WriteLine("Please enter the cash amount you are paying with..");

            var money = Console.ReadLine();
            var a = 0;
            bool value = int.TryParse(money, out a);

            if (value == true)
            {
                int d = int.Parse(money);
                var change = d - total;

                Console.WriteLine();
                Console.WriteLine("----------------------------------------------------------------------------------");
                Console.WriteLine();
                Console.WriteLine($"You recieve ${change} in change.");
                Console.WriteLine();
                Console.WriteLine("Thank you for shopping at The Pizza Shop, have a nice day and enjoy your order :)");
                Console.ReadLine();
            }

            else
            {
                Console.WriteLine("Sorry that's not a number, please try again.");
                Console.ReadLine();
                payment();
            }
        }
    }
}